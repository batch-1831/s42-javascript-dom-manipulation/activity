// console.log("hello werld");

// [SECTION] Document Object Model (DOM)
// it allows us to be able to access or modify the properties of an element in a webpage.
// it is a standard on how to get, change, add, or delete HTML elements.
/*
    Syntax: 
        document.querySelector("htmlElement");
    
        -The querySelector fuction takes a string input that is formatted like a CSS Selector when applying styles
*/
// Alternative way on retrieving HTML elements
// document.getElementById("txt-first-name");
// document.getElementsByClassName("txt-last-name");

// However, using these functions requires us to identify beforehand we get the element/ With querySelector, we can be flexible in how to retrieve elements.

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event Listeners
// Whenever a user interacts with a web page, this action is considered as an event. (example: mouse click, mouse hover, page load, key press, etc.)

// addEventListener take two argument:
    // a string that identify an event/
    // a function that the listener will execute once the "specified  event" is triggered.
// txtFirstName.addEventListener("keyup", () => {
//     // "innerHTML" property sets or returns the HTML content(inner HTML) of an element (div, spans, etc.)
//     // ".value" property sets or returns the value of an attribute (form controls)
//     spanFullName.innerHTML = `${txtFirstName.value}`;
// })

// txtFirstName.addEventListener("keyup", (event) => {
//     // The "event.target" contains the element
//     console.log(event.target);
//     // The "event.target.value" gets the value of the input object(txt-first-name)
//     console.log(event.target.value);
// })

// Crete Multiple event that use the same function.
const selectColor = document.querySelector("#colors");
    // console.log(selectColor.value)
function fullName() {

    if (selectColor.value == "green") {
        spanFullName.style.color = "green";
        spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
    }
    else if (selectColor.value == "blue") {
        spanFullName.style.color = "blue";
        spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
    }
    else {
        spanFullName.style.color = "red";
        spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
    }
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);


//Activity
/*
Create another addEventListener that will "change the color" of the "spanFullName". Add a "select tag" element with the options/values of red, green, and blue in your index.html.

Check the following links to solve this activity:
	HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
	HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/
